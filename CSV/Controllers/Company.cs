﻿namespace CSV.Controllers
{
    internal class Company
    {
        public string Name { get; set; }
        public string Designation { get; set; }
    }
}