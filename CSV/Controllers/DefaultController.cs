﻿using CsvHelper;
using Excel;
using LinqToExcel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CSV.Controllers
{
    public class DefaultController : Controller
    {
        // GET: Default
        public ActionResult Index()
        {

            var file = Server.MapPath(@"~/App_Data/Employee.xlsx");

            using (FileStream stream = new FileStream(file, FileMode.Open, FileAccess.Read))
            {
                byte[] bytes = new byte[stream.Length];
                IExcelDataReader reader = ExcelReaderFactory.CreateOpenXmlReader(stream);


                //treats the first row of excel file as Coluymn Names
                reader.IsFirstRowAsColumnNames = true;
                //Adding reader data to DataSet()
                DataSet result = reader.AsDataSet();

                reader.Close();


                var Table = result.Tables[0];
                var x = Table.Rows[0][1];

                //column headers
                foreach (DataColumn column in Table.Columns)
                {
                    var columnName = column.ColumnName;
                }

                //column values tr
                foreach (DataRow row in Table.Rows)
                {
                    //actual values td
                    foreach (DataColumn col in Table.Columns)
                    {
                        var xxx = row[col.ColumnName];
                    }
                }
            }





            //download as 
            //List<string> tbl = new List<string>();
            //tbl.Add("first");
            //tbl.Add("Second");

            //using (ExcelPackage pck = new ExcelPackage())
            //{
            //    //Create the worksheet
            //    ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Demo");

            //    //Load the datatable into the sheet, starting from cell A1. Print the column names on row 1
            //    ws.Cells["A1"].LoadFromCollection(tbl);

            //    //Format the header for column 1-3
            //    using (ExcelRange rng = ws.Cells["A1:C1"])
            //    {
            //        rng.Style.Font.Bold = true;
            //        rng.Style.Fill.PatternType = ExcelFillStyle.Solid;                      //Set Pattern for the background to Solid
            //        rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(79, 129, 189));  //Set color to dark blue
            //        rng.Style.Font.Color.SetColor(Color.White);
            //    }

            //    //Example how to Format Column 1 as numeric 
            //    using (ExcelRange col = ws.Cells[2, 1, 2 + tbl.Count(), 1])
            //    {
            //        col.Style.Numberformat.Format = "#,##0.00";
            //        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            //    }

            //    //Write it back to the client
            //    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            //    Response.AddHeader("content-disposition", "attachment;  filename=ExcelDemo.xlsx");
            //    Response.BinaryWrite(pck.GetAsByteArray());
            //}


            //using (CsvFileReader reader = new CsvFileReader(Server.MapPath(@"~/App_Data/WriteTest.csv")))
            //{

            //    var csv = new CsvReader(reader);
            //    var records = csv.GetRecords<MyClass>().ToList();


            //}




            //// Read sample data from CSV file
            //using (CsvFileReader reader = new CsvFileReader(Server.MapPath(@"~/App_Data/WriteTest.csv")))
            //{
            //    CsvRow row = new CsvRow();
            //    while (reader.ReadRow(row))
            //    {
            //        foreach (string s in row)
            //        {
            //            Console.Write(s);
            //            Console.Write(" ");
            //        }
            //        Console.WriteLine();
            //    }
            //}


            //// Write sample data to CSV file
            //using (CsvFileWriter writer = new CsvFileWriter(Server.MapPath(@"~/App_Data/WriteTest.csv")))
            //{
            //    for (int i = 0; i < 100; i++)
            //    {
            //        CsvRow row = new CsvRow();
            //        for (int j = 0; j < 5; j++)
            //            row.Add(String.Format("Column{0}", j));
            //        writer.WriteRow(row);
            //    }
            //}

            return View();
        }
    }
}